<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-domain-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PHPUnit\Framework\TestCase;

/**
 * DomainTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Domain\Domain
 *
 * @internal
 *
 * @small
 */
class DomainTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Domain
	 */
	protected Domain $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('www.example.com', $this->_object->__toString());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testIsTooLong() : void
	{
		$this->expectException(LengthException::class);
		
		new Domain(\explode('.', 'azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn'));
	}
	
	public function testGetDepths() : void
	{
		$this->assertEquals(3, $this->_object->getDepths());
	}
	
	public function testAppend() : void
	{
		$this->assertEquals('toto.www.example.com', $this->_object->append('toto')->__toString());
	}
	
	public function testAppendMultiple() : void
	{
		$this->assertEquals('toto2.toto.www.example.com', $this->_object->append('toto2.toto')->__toString());
	}
	
	public function testAppendEmpty() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->append('');
	}
	
	public function testAppendTooLong() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->append('azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqX');
	}
	
	public function testAppendNotTooLong() : void
	{
		$this->assertEquals('azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopq.www.example.com', $this->_object->append('azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopq')->__toString());
	}
	
	public function testAppendDomainTooLong() : void
	{
		$this->expectException(LengthException::class);
		
		$this->_object->append('azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn.azertyuiopqsdfghjklmwxcvbn');
	}
	
	public function testAppendDoesNotBeginWithLetter() : void
	{
		$this->assertEquals('123abc.www.example.com', $this->_object->append('123abc'));
	}
	
	public function testAppendDoesNotEndsWithLetterOrDigit() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->append('abc---');
	}
	
	public function testAppendMixed() : void
	{
		$this->assertEquals('abc123----123abc.www.example.com', $this->_object->append('abc123----123abc'));
	}
	
	public function testAppendInvalidCharacter() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->append('abc{}def');
	}
	
	public function testBuildInvalid() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		new Domain(['abc', 'def', '#%=', 'com']);
	}
	
	public function testUnderscore() : void
	{
		$this->assertEquals('mairie_osseyles3maisons.pagesperso-orange.fr', (string) new Domain('mairie_osseyles3maisons.pagesperso-orange.fr'));
	}
	
	public function testGetParent() : void
	{
		$this->assertEquals('example.com', $this->_object->getParent()->__toString());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('www.example.com', $this->_object->getCanonicalRepresentation());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Domain('www.example.com');
	}
	
}
