<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-domain-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Domain;

use InvalidArgumentException;
use LengthException;

/**
 * Domain class file.
 * 
 * This class is a simple implementation of the DomainInterface.
 * 
 * @author Anastaszor
 */
class Domain implements DomainInterface
{
	
	/**
	 * The parts of the domain. Order is :
	 * www.example.com -> [0 => 'www', 1 => 'example', 2 => 'com'].
	 * 
	 * @var array<integer, string>
	 */
	protected array $_parts = [];
	
	/**
	 * Builds a new Domain with the given parts.
	 * 
	 * @param string|array<integer, string> $parts
	 * @throws InvalidArgumentException
	 * @throws LengthException
	 */
	public function __construct($parts = [])
	{
		if(\is_string($parts))
		{
			$parts = \explode('.', $parts);
		}
		
		$length = \max(0, \count($parts) - 1);
		
		foreach($parts as $part)
		{
			$length += \strlen($part);
		}
		
		if(255 < $length)
		{
			throw new LengthException('The domain name "'.\implode('.', $parts).'" exceeds the 255 char limit on domain names.');
		}
		
		try
		{
			foreach($parts as $k => $part)
			{
				$this->_parts[] = $this->checkPart($k, $part);
			}
		}
		catch(InvalidArgumentException $e)
		{
			$message = 'Failed to set domain "{domain}", validity check failed : {msg}';
			$context = ['{domain}' => \implode('.', $parts), '{msg}' => $e->getMessage()];
			
			throw new InvalidArgumentException(\strtr($message, $context), -1, $e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Domain\DomainInterface::append()
	 */
	public function append(string $part) : DomainInterface
	{
		if(255 < \strlen($part) + $this->getLength())
		{
			throw new LengthException('The given part "'.$part.'" added to this domain "'.$this->getCanonicalRepresentation().'" exceed the 255 char limit for a domain name.');
		}
		
		$newParts = $this->_parts;
		
		// handle the case when a multiple levels are given
		foreach(\array_reverse(\explode('.', $part)) as $newPart)
		{
			try
			{
				\array_unshift($newParts, $this->checkPart(0, $newPart));
			}
			catch(InvalidArgumentException $e)
			{
				$message = 'Failed to append part "{part}", validity check failed : {msg}';
				$context = ['{part}' => $part, '{msg}' => $e->getMessage()];
				
				throw new InvalidArgumentException(\strtr($message, $context), -1, $e);
			}
		}
		
		return new self($newParts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Domain\DomainInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return \implode('.', $this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Domain\DomainInterface::getDepths()
	 */
	public function getDepths() : int
	{
		return \count($this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Domain\DomainInterface::getLength()
	 */
	public function getLength() : int
	{
		$count = \max(0, $this->getDepths() - 1);
		
		foreach($this->_parts as $part)
		{
			$count += \strlen($part);
		}
		
		return $count;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Domain\DomainInterface::getParent()
	 * @throws InvalidArgumentException
	 * @throws LengthException
	 */
	public function getParent() : DomainInterface
	{
		return new self(\array_slice($this->_parts, 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Domain\DomainInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return $this->getDepths() === 0;
	}
	
	/**
	 * Checks whether the given part is valid.
	 * 
	 * @param integer $partNb
	 * @param string $part
	 * @return string
	 * @throws InvalidArgumentException
	 */
	protected function checkPart(int $partNb, string $part) : string
	{
		// RFC1034: <label> ::= <letter> [ [ <ldh-str> ] <let-dig> ]
		
		$len = (int) \mb_strlen($part);
		if(0 >= $len)
		{
			$message = 'The given part nb. {k} should not be empty.';
			$context = ['{k}' => $partNb + 1];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(64 <= $len)
		{
			$message = 'The given part nb. {k} "{domain}" should be less than 64 characters, {len} given.';
			$context = ['{k}' => $partNb + 1, '{domain}' => $part, '{len}' => $len];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		for($i = 0; $i < $len; $i++)
		{
			$chr = $part[$i];
			$ord = \ord($chr);
			// used to be only letters as the rfc1035 specifies
			// but there are real domains that begin with a digit
			if(0 === $i) // first character, only letters (or digits)
			{
				if(!$this->isLetterOrDigit($ord))
				{
					$message = 'The given character {i} of part nb. {k} should be a letter, "{chr}" given.';
					$context = ['{i}' => $i + 1, '{k}' => $partNb + 1, '{chr}' => $chr];
					
					throw new InvalidArgumentException(\strtr($message, $context));
				}
				
				continue;
			}
			
			if($len - 1 === $i) // last character
			{
				if(!$this->isLetterOrDigit($ord))
				{
					$message = 'The given character {i} of part nb. {k} should be a letter or a digit, "{chr}" given.';
					$context = ['{i}' => $i + 1, '{k}' => $partNb + 1, '{chr}' => $chr];
					
					throw new InvalidArgumentException(\strtr($message, $context));
				}
				
				continue;
			}
			
			if(!$this->isLetterOrDigitOrHyphen($ord))
			{
				$message = 'The given character {i} of part nb. {k} should be a letter, digit or hyphen, "{chr}" given.';
				$context = ['{i}' => $i + 1, '{k}' => $partNb + 1, '{chr}' => $chr];
				
				throw new InvalidArgumentException(\strtr($message, $context));
			}
		}
		
		return (string) \mb_strtolower($part);
	}
	
	/**
	 * Gets whether the given ascii code is a letter or digit or hyphen.
	 * 
	 * @param integer $ord
	 * @return boolean
	 */
	protected function isLetterOrDigitOrHyphen(int $ord) : bool
	{
		return $this->isHyphen($ord) || $this->isLetterOrDigit($ord);
	}
	
	/**
	 * Gets whether the given ascii code is a letter or digit.
	 * 
	 * @param integer $ord
	 * @return boolean
	 */
	protected function isLetterOrDigit(int $ord) : bool
	{
		return $this->isLetter($ord) || $this->isDigit($ord);
	}
	
	/**
	 * Gets whether the given ascii code is a letter.
	 * 
	 * @param integer $ord
	 * @return boolean
	 */
	protected function isLetter(int $ord) : bool
	{
		return (65 <= $ord && 90 >= $ord) || (97 <= $ord && 122 >= $ord);
	}
	
	/**
	 * Gets whether the given ascii code is a digit.
	 * 
	 * @param integer $ord
	 * @return boolean
	 */
	protected function isDigit(int $ord) : bool
	{
		return 48 <= $ord && 57 >= $ord;
	}
	
	/**
	 * Gets whether the given ascii code is an hyphen.
	 * 
	 * @param integer $ord
	 * @return boolean
	 */
	protected function isHyphen(int $ord) : bool
	{
		// 95 is underscore, some domain names use it (for real)
		return 45 === $ord || 95 === $ord;
	}
	
}
